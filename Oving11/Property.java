
/**
 * A class representing a property
 */
public class Property {
    private final int municipalityNumber;
    private final String municipalityName;
    private final int propertyNumber;
    private final int sectionNumber;
    private final double area;
    private String owner;
    private String name;

    /**
     * Constructor for objects of class Property
     *
     * @param municipalityNumber the municipality number
     * @param municipalityName the municipality name
     * @param propertyNumber the property number
     * @param sectionNumber the section number
     * @param area the area of the property
     * @param owner the owner of the property
     * @param name the name of the property
     */
    public Property(int municipalityNumber, String municipalityName, int propertyNumber, int sectionNumber, double area, String owner, String name) {
        this.municipalityNumber = municipalityNumber;
        this.municipalityName = municipalityName;
        this.propertyNumber = propertyNumber;
        this.sectionNumber = sectionNumber;
        this.area = area;
        this.owner = owner;
        this.name = name;
    }

    /**
     * Constructor for objects of class Property
     *
     * @param municipalityNumber the municipality number
     * @param municipalityName the municipality name
     * @param propertyNumber the property number
     * @param sectionNumber the section number
     * @param area the area of the property
     * @param owner the owner of the property
     */
    public Property(int municipalityNumber, String municipalityName, int propertyNumber, int sectionNumber, double area, String owner) {
        this.municipalityNumber = municipalityNumber;
        this.municipalityName = municipalityName;
        this.propertyNumber = propertyNumber;
        this.sectionNumber = sectionNumber;
        this.area = area;
        this.owner = owner;
        this.name = "";
    }

    /**
     * Retrieves the municipality number
     * @return the municipality number
     */
    public int getMunicipalityNumber() {
        return this.municipalityNumber;
    }

    /**
     * Retrieves the municipality name
     * @return the municipality name
     */
    public String getMunicipalityName() {
        return this.municipalityName;
    }

    /**
     * Retrieves the property number
     * @return the property number
     */
    public int getPropertyNumber() {
        return this.propertyNumber;
    }

    /**
     * Retrieves the section number
     * @return the section number
     */
    public int getSectionNumber() {
        return this.sectionNumber;
    }

    /**
     * Retrieves the name of the property
     * @return the name of the property
     */
    public String getName() {
        return this.name;
    }

    /**
     * Retrieves the area of the property
     * @return the area of the property
     */
    public double getArea() {
        return this.area;
    }

    /**
     * Retrieves the owner of the property
     * @return the owner of the property
     */
    public String getOwner() {
        return this.owner;
    }

    /**
     * Retrieves the id number of the property
     * @return the id number of the property
     */
    public String getIdNumber() {
        return String.format("%d-%d/%d", this.getMunicipalityNumber(), this.getPropertyNumber(), this.getSectionNumber());
    }

    /**
     * Sets the owner of the property
     * @param owner the owner of the property
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * Sets the name of the property
     * @param name the name of the property
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-15s | %-20s | %-15s | %-15.2f | %-15s",
                this.getMunicipalityName(), this.getIdNumber(), this.getName(), this.getArea(), this.getOwner());
    }
}
