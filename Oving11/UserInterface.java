import java.util.Scanner;

/**
 * A class to represent a user interface
 */
public class UserInterface {
    private PropertyRegister propertyRegister;
    private final Scanner scanner = new Scanner(System.in);

    /**
     * Constructor for objects of class UserInterface
     */
    public UserInterface() {
        this.propertyRegister = new PropertyRegister();
        this.addTestData();
    }

    /**
     * Starts the user interface. main logic of the program
     */
    public void start(){
        int option;

        while (true){
            option = mainMenu();
            if (option == 0){
                break;
            }
            switch (option){
                case 1:
                    this.addProperty();
                    break;
                case 2:
                    this.removeProperty();
                    break;
                case 3:
                    this.getPropertyFromIdNumber();
                    break;
                case 4:
                    System.out.printf("Average area is %s m^2", this.propertyRegister.getAverageArea());
                    break;
                case 5:
                    this.getPropertiesByPropertyNumber();
                    break;
                case 6:
                    this.displayProperties();
                    break;
                default:
                    System.out.println("Invalid option");
            }
        }
    }

    /**
     * Displays the main menu and returns the selected option
     *
     * @return the selected option
     */
    public int mainMenu(){
        int option;
        while (true){
            System.out.println("\n[1] Add property");
            System.out.println("[2] Remove property");
            System.out.println("[3] Get property by id number");
            System.out.println("[4] Get average area");
            System.out.println("[5] Get properties by property number");
            System.out.println("[6] Display properties");
            System.out.println("[0] Exit");
            System.out.print("Enter option: ");
            try {
                option = Integer.parseInt(scanner.nextLine());
                return option;
            } catch (NumberFormatException e) {
                System.out.println("Invalid option, input must be an integer");
            }
        }
    }

    /**
     * Adds a property to the property register
     */
    public void addProperty() {
        Property property;
        int municipalityNumber;
        String municipalityName;
        int propertyNumber;
        int sectionNumber;
        double area;
        String owner;
        String name;
        String option;

        System.out.print("Enter municipality number: ");
        municipalityNumber = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter municipality name: ");
        municipalityName = scanner.nextLine();
        System.out.print("Enter property number: ");
        propertyNumber = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter section number: ");
        sectionNumber = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter area: ");
        area = Double.parseDouble(scanner.nextLine());
        System.out.print("Enter owner: ");
        owner = scanner.nextLine();
        System.out.println("Enter name? [y/n]");
        option = scanner.nextLine();
        if (option.equals("y")) {
            System.out.print("Enter name: ");
            name = scanner.nextLine();
            property = new Property(municipalityNumber, municipalityName, propertyNumber, sectionNumber, area, owner, name);
        } else {
            property = new Property(municipalityNumber, municipalityName, propertyNumber, sectionNumber, area, owner);
        }
        this.propertyRegister.addProperty(property);
    }

    /**
     * Removes a property from the property register
     */
    public void removeProperty(){
        int option;
        while (true){
            this.displayPropertiesInMenu();
            System.out.print("Enter option: ");
            try {
                option = Integer.parseInt(scanner.nextLine());
                if (option < 1 || option > this.propertyRegister.getNumberOfProperties()){
                    throw new Exception();
                }
                break;
            } catch (Exception e) {
                System.out.printf("Invalid option, input must be an integer between 1 and %d\n",
                        this.propertyRegister.getNumberOfProperties());
            }
        }
        this.propertyRegister.removeProperty(option-1);
    }

    /**
     * Retrieves a property from the property register by id number
     */
    public void getPropertyFromIdNumber() {
        String idNumber;
        System.out.print("Enter id number: ");
        idNumber = scanner.nextLine();
        System.out.println(this.propertyRegister.getPropertyFromIdNumber(idNumber));
    }

    /**
     * Retrieves properties from the property register by property number
     */
    public void getPropertiesByPropertyNumber() {
        int propertyNumber;
        while (true){
            System.out.print("Enter property number: ");
            try {
                propertyNumber = Integer.parseInt(scanner.nextLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid option, input must be an integer");
            }
        }
        System.out.println(this.propertyRegister.getPropertiesByPropertyNumber(propertyNumber));
    }

    /**
     * Displays all properties in the property register
     */
    public void displayProperties() {
        System.out.println(this.propertyRegister);
    }

    /**
     * Displays all properties in the property register in the menu
     */
    public void displayPropertiesInMenu() {
        System.out.println(this.propertyRegister.displayProperties());
    }

    /**
     * Adds test data to the property register
     */
    private void addTestData(){
        Property property;
        property = new Property(1445, "Gloppen", 77, 631, 1017.6, "Jens Olsen");
        this.propertyRegister.addProperty(property);
        property = new Property(1445, "Gloppen", 77, 131, 661.3, "Nicolay Madsen", "Syningom");
        this.propertyRegister.addProperty(property);
        property = new Property(1445, "Gloppen", 75, 19, 650.6, "Evilyn Jensen", "Fugletun");
        this.propertyRegister.addProperty(property);
        property = new Property(1445, "Gloppen", 74, 188, 1457.2, "Karl Ove Bråten");
        this.propertyRegister.addProperty(property);
        property = new Property(1445, "Gloppen", 69, 47, 1339.4, "Elsa Indregård", "Høiberg");
        this.propertyRegister.addProperty(property);
    }
}
