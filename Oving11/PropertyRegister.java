import java.util.ArrayList;

/**
 * A class to represent a property register
 */
public class PropertyRegister {
    private final ArrayList<Property> properties;

    /**
     * Constructor for objects of class PropertyRegister
     */
    public PropertyRegister() {
        this.properties = new ArrayList<>();
    }

    /**
     * Constructor for objects of class PropertyRegister
     *
     * @param properties the properties
     */
    public PropertyRegister(ArrayList<Property> properties) {
        this.properties = properties;
    }

    /**
     * Retrieves the properties
     *
     * @return the properties
     */
    public ArrayList<Property> getProperties() {
        return properties;
    }

    /**
     * Adds a property to the register
     *
     * @param property the property to add
     */
    public void addProperty(Property property) {
        this.properties.add(property);
    }

    /**
     * Removes a property from the register
     *
     * @param index the index of the property to remove
     */
    public void removeProperty(int index) {
        this.properties.remove(index);
    }

    /**
     * Retrieves the number of properties
     *
     * @return the number of properties
     */
    public int getNumberOfProperties() {
        return this.getProperties().size();
    }

    /**
     * Retrieves a property
     *
     * @param index the index of the property to retrieve
     *
     * @return the property
     */
    public Property getProperty(int index) {
        return this.getProperties().get(index);
    }

    /**
     * Retrieves a property from the id number
     *
     * @param idNumber the id number of the property to retrieve
     *
     * @return the property
     */
    public Property getPropertyFromIdNumber(String idNumber) {
        for (Property property : this.getProperties()) {
            if (property.getIdNumber().equals(idNumber)) {
                return property;
            }
        }
        return null;
    }

    /**
     * Retrieves the average area of the properties
     *
     * @return the average area of the properties
     */
    public double getAverageArea() {
        double totalArea = 0;
        for (Property property : this.getProperties()) {
            totalArea += property.getArea();
        }
        return totalArea / this.getNumberOfProperties();
    }

    /**
     * Retrieves the properties with the given property number
     *
     * @param propertyNumber the property number
     *
     * @return the properties with the given property number
     */
    public PropertyRegister getPropertiesByPropertyNumber(int propertyNumber) {
        ArrayList<Property> properties = new ArrayList<>();
        for (Property property : this.getProperties()) {
            if (property.getPropertyNumber() == propertyNumber) {
                properties.add(property);
            }
        }
        return new PropertyRegister(properties);
    }

    /**
     * Returns a string representation of the properties
     *
     * @return a string representation of the properties
     */
    public String displayProperties() {
        String output = "";
        for (int i = 0; i < this.getProperties().size(); i++) {
            output += String.format("[%d] %s\n", i+1, this.getProperty(i).getIdNumber());
        }
        return output;
    }

    @Override
    public String toString() {
        String output = "";
        output += String.format("%-15s | %-20s | %-15s | %-15s | %-15s\n", "Municipality", "Id number", "Name", "Area m^2", "Owner");
        for (Property property : this.getProperties()) {
            output += property.toString() + "\n";
        }
        return output;
    }
}
