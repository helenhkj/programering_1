// Oppgave 1
// Lag en klasse Valuta med minst en konstruktør. Klassen skal ha metoder for å regne fra og til norske kroner.
// Lag et klientprogram som oppretter minst tre objekter som representerer forskjellige valutaer. Brukeren skal få tilbud om å regne om flere ulike beløp i de forskjellige valutaene til norske kroner.

package oving_4;

import java.util.Scanner;

public class Opp1_oving4 {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        Valuta euro = new Valuta(11.4913, "Euro");
        Valuta usd = new Valuta(10.6612, "Dollar");
        Valuta sek = new Valuta(0.9659, "Svenske kroner");
        Valuta[] currencies = {euro, usd, sek};
        int option;
        double value;
        boolean convert_to;
        String choice;

        while (true){
            System.out.println("Velg valutta");
            for (int i = 0; i < currencies.length; i++) {
                System.out.printf("%d. %s %n", i+1, currencies[i].name);
            }
            System.out.printf("%d. %s %n", currencies.length + 1, "Avslutt");
            
            option = tryCatchInt();
            if (option == 4){
                break; // avslutt
            }
            else if (1 <= option && option <= currencies.length){
                System.out.println("Konverter til Norske kroner? [y/n]");
                choice = scannerObj.next();
                if (choice.equals("y")){
                    convert_to = true;
                }
                else {
                    convert_to = false;
                }
                System.out.println("Skriv inn en sum");
                value = scannerObj.nextDouble();
                for (int i = 0; i < currencies.length; i++) {
                    if (option == i+1){
                        if (convert_to){
                            System.out.printf("%n%.2f %s er lik %.2f norske kroner %n%n", value, currencies[i].name, currencies[i].convert_to_nok(value));
                        }
                        else {
                            System.out.println(currencies[i].convert_nok_to(value));
                            System.out.printf("%n%.2f norske kroner er lik %.2f %s %n%n", value, currencies[i].convert_nok_to(value), currencies[i].name);
                        }
                    }
                }
            }
        }
    }

    private static int tryCatchInt() {
        Scanner scannerObj = new Scanner(System.in);
        int var = 0;
        try {
            var = scannerObj.nextInt();
        } 
        catch (Exception e) {
            System.out.println("Skriv inn et heltall");
            scannerObj.next();
            return tryCatchInt();
        }
        return var;
    }
}

class Valuta {
    double rate;
    String name;

    public Valuta(double rate_arg, String name_arg){
        rate = rate_arg;
        name = name_arg;
    }

    /**
     * 
     * @param number
     * @return
     */
    public Double convert_to_nok(double number){
        return number*rate;
    }

    public Double convert_nok_to(double number){
        return number/rate;
    }
}
