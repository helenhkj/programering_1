package oving_4;

import java.util.Random;

public class Opp2_oving4 {
    public static void main(String[] args) {
        Player player_a = new Player("A");
        Player player_b = new Player("B");
        Player[] players = {player_a, player_b};
        boolean continue_game = true;

        while (continue_game){
            Player.round ++;
            System.out.printf("%n%d. runde%n", Player.round);
            for (Player player : players) {
                player.throw_dice();
                if (!Player.game){
                    continue_game = false;
                    break;
                }
            }
        }
    }
}

class Player {
    // klasse atributter
    static int round = 0;
    static Random dice = new Random();
    static boolean game = true;
    static int max_point = 100;

    // instans atributter
    int sum_points;
    String name;

    /**
     * Konstruktør
     * @param name_arg
     */
    public Player(String name_arg) {
        sum_points = 0;
        name = name_arg;
    }

    public void throw_dice(){
        int result = dice.nextInt(6) + 1;
        if (result == 1){
            sum_points = 0;
        }
        else if (sum_points < max_point){
            sum_points += result;
        }
        else{
            sum_points -= result;
        }
        print_result(result);
        if (sum_points == max_point){
            won_game();
        }
    }

    private void print_result(int result) {
        System.out.printf("Spiller %s kastet %d og har %d poeng %n", name, result, sum_points);
    }

    private void won_game() {
        System.out.printf("Spiller %s vant spillet med %d poeng %n", name, sum_points);
        game = false;
    }
}