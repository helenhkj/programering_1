// Oppgave 2
// Skriv et program som finner ut om et tall er et primtall. Et primtall er et tall som kun kan deles med 1 og med seg selv uten å få rest. Les inn tallet fra brukeren og la programmet repetere slik at flere tall kan analyseres.

package oving_3;

import java.util.Scanner;

public class Opp2_oving3 {
    public static void main(String[] args) {
        while (true){
            System.out.println("");
            System.out.println("Skriv inn ett heltall:");
            int number = tryCatchInt();
            if (primeNumber(number)){
                System.out.println(number + " er et primtall.");
            }
            else {
                System.out.println(number + " er ikke et primtall.");
            }

            Scanner scannerObj = new Scanner(System.in);
            System.out.println("Vil du forsette [y/n]");
            String choice = scannerObj.nextLine();
            if (choice.contains("n")){
                break;
            }
        }
    }

    private static Boolean primeNumber(int number){
        for (int i = 2; i < number; i++){
            if (number%i == 0){
                return false;
            }
        }
        return true;
    }

    private static int tryCatchInt() {
        Scanner scannerObj = new Scanner(System.in);
        int var = 0;
        try {
            var = scannerObj.nextInt();
        } 
        catch (Exception e) {
            System.out.println("Skriv inn et heltall");
            scannerObj.next();
            return tryCatchInt();
        }
        return var;
    }
}
