// Oppgave 1:
// Lag et program som skriver ut en del av multiplikasjonstabellen, for eksempel fra 13-15. Da skal utskriften se omtrent slik ut (prikkene skal erstattes med regnestykker).

package oving_3;

import java.util.Scanner;

public class Opp1_oving3 {
    public static void main(String[] args) {
        int lowerLimit = 0;
        int upperLimit = 0;

        System.out.println("Skriv inn nedre grense for ønsket intervall:");
        lowerLimit = tryCatchInt();

        System.out.println("Skriv inn øvre grense for ønsket intervall:");
        upperLimit = tryCatchInt();

        table(lowerLimit, upperLimit);
    }

    private static void table(int lowerLimit, int upperLimit){
        for (int i = lowerLimit; i <= upperLimit; i++){
            System.out.println(i+"-gangen");
            for (int y = 1; y <= 10; y++){
                System.out.println(i + " x " + y + " = " + i*y);
            }
        }
    }

    private static int tryCatchInt() {
        Scanner scannerObj = new Scanner(System.in);
        int var = 0;
        try {
            var = scannerObj.nextInt();
        } 
        catch (Exception e) {
            System.out.println("Skriv inn et heltall");
            scannerObj.next();
            return tryCatchInt();
        }
        return var;
    }

    // private static int tryCatchInt() {
    //     boolean run = true;
    //     Scanner scannerObj = new Scanner(System.in);
    //     int var = 0;
    //     do{
    //         try {
    //             var = scannerObj.nextInt();
    //             break;
    //         } 
    //         catch (Exception e) {
    //             System.out.println("Skriv inn et heltall");
    //             scannerObj.next();
    //         }
    //     } while(run);
    //     return var;
    // }
}
