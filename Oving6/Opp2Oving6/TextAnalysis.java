package oving6.Opp2Oving6;

/**
 * Klasse som analyserer en tekst
 * @attribute symbols array med alle bokstavene og et symbol
 * @attribute numberSybols array med antall av hver bokstav og symbol
 * @method getNumberOfDifrentLetters() returnerer antall ulike bokstaver
 * @method getNumberOfSymbolsTotal() returnerer antall bokstaver totalt
 * @method getPercentageOfNotLetters() returnerer prosentandelen av teksten som ikke er bokstaver
 * @method getNumberOfLetter(String letter) returnerer antall av en bokstav
 * @method getMostFrequentLetters() returnerer den/de mest brukte bokstaven(e)
 */
public class TextAnalysis {
    // Klasseattributter
    private static String[] symbols = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "æ", "ø", "å", "symbol"};
    // Instansattributter
    private int numberSybols[] = new int[30];

    /**
     * Konstruktør
     * @param text
     */
    public TextAnalysis(String text){
        text = text.toLowerCase();
        System.out.println(text);

        // Fyller arrayet med antall av hver bokstav
        for (int i = 0; i < text.length(); i++) {
            int charValue = text.charAt(i);

            // sjekker om charValue er en bokstav
            if (charValue >= 97 && charValue <= 122){
                for (int j = 0; j < symbols.length-1; j++) {
                    if (symbols[j].equals(Character.toString(charValue))){
                        this.numberSybols[j] += 1;
                    }
                }
            }
            else if(charValue == 230){
                this.numberSybols[26] += 1;
            }
            else if(charValue == 248){
                this.numberSybols[27] += 1;
            }
            else if(charValue == 229){
                this.numberSybols[28] += 1;
            }
            else{
                this.numberSybols[29] += 1;
            }
        }
    }

    /**
     * Metode som returnerer antall ulike bokstaver
     * @return int
     */
    public Integer getNumberOfDifrentLetters(){
        int total = 0;
        for (int i = 0; i < this.numberSybols.length-1; i++) {
            if (this.numberSybols[i] > 0){
                total += 1;
            }
        }
        return total;
    }

    /**
     * Metode som returnerer antall bokstaver totalt
     * @return
     */
    public Integer getNumberOfLettersTotal(){
        int total = 0;
        for (int i = 0; i < this.numberSybols.length-1; i++) {
            total += this.numberSybols[i];
        }
        return total;
    }

    /**
     * Metode som returnerer prosentandelen av teksten som ikke er bokstaver
     * @return
     */
    public Double getPercentageOfNotLetters(){
        double notLetters = this.numberSybols[29];
        double total = getNumberOfLettersTotal() + notLetters;
        return (double) (notLetters/total)*100;
    }

    /**
     * Metode som returnerer antall av en bokstav
     * @param letter
     * @return
     */
    public Double getNumberOfLetter(String letter){
        int output = 0;
        for (int i = 0; i < symbols.length; i++) {
            if (symbols[i].equals(letter)){
                output = this.numberSybols[i];
            }
        }
        return (double) output;
    }

    /**
     * Metode som returnerer den/de mest brukte bokstaven(e)
     * @return
     */
    public String getMostFrequentLetters(){
        int max = 0;
        String output = "";
        for (int i = 0; i < this.numberSybols.length-1; i++) {
            if (this.numberSybols[i] > max){
                max = this.numberSybols[i];
            }
        }
        for (int i = 0; i < this.numberSybols.length-1; i++) {
            if (this.numberSybols[i] == max){
                output += symbols[i] + ", ";
            }
        }
        return output;
    }
}