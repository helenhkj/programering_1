package oving6.Opp2Oving6;

import java.util.Scanner;

public class Opp2Oving6 {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);

        while (true){
            String input;
            System.out.println("\nSkriv inn en tekst:");
            input = scannerObj.nextLine();
            if (input.equals("")){
                break;
            }
            TextAnalysis textAnalysisObj = new TextAnalysis(input);
            System.out.printf("Antall ulike bokstaver: %d\n",textAnalysisObj.getNumberOfDifrentLetters());
            System.out.printf("Antall bokstaver totalt: %d\n",textAnalysisObj.getNumberOfLettersTotal());
            System.out.printf("%.2f%% av teksten er ikke bokstaver\n",textAnalysisObj.getPercentageOfNotLetters());
            System.out.println("Skriv inn en bokstav:");
            while (true){
                input = scannerObj.nextLine();
                if (input.length() == 1){
                    int letter = input.charAt(0);
                    if ((letter >= 97 && letter <= 122) || letter == 230 || letter == 248 || letter == 229){
                        break;
                    }
                }
                System.out.println("Skriv inn en bokstav:");
            }
            System.out.printf("Antall av bokstaven %s: %s\n", input, textAnalysisObj.getNumberOfLetter(input.toLowerCase()));
            System.out.printf("Mest brukte bokstav(er): %s\n", textAnalysisObj.getMostFrequentLetters());
        }
    }
}
