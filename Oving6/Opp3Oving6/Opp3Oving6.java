package oving6.Opp3Oving6;

import java.util.Scanner;
import java.util.ArrayList;

public class Opp3Oving6 {
    /**
     * Main-metode
     * @param args
     */
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        ArrayList<Matrix> objects = new ArrayList<Matrix>();
        String[] operations = {"Adder to matriser", "Multipliser to matriser", "Transponer en matrise"}; 
        int option;
        Matrix matrix1;
        Matrix matrix2;
        Matrix matrix3;
        
        while (true){
            System.out.println("Velg en operasjon:");
            for (int i = 0; i < operations.length; i++) {
                System.out.printf("[%d] %s\n", i+1, operations[i]);
            }
            System.out.printf("[%d] Avslutt\n", operations.length+1);
            option = scannerObj.nextInt();

            if (option == operations.length+1){
                break;
            }

            System.out.println("\nVelg første matrise:");
            matrix1 = chooseMatrix(objects);
            
            switch (option) {
                case 1:
                    System.out.println("\nVelg andre matrise:");
                    matrix2 = chooseMatrix(objects);
                    matrix3 = matrix1.addMatrix(matrix2);
                    if (matrix3 == null){
                        System.out.println("Matrisene kan ikke adderes\n");
                    }
                    else {
                        System.out.printf("\nMatrise 1:\n%s\nMatrise 2:\n%s\nSum:\n%s\n", matrix1.displayMatrix(), matrix2.displayMatrix(),matrix3.displayMatrix());
                    }
                    break;
                case 2:
                    System.out.println("\nVelg andre matrise:");
                    matrix2 = chooseMatrix(objects);
                    matrix3 = matrix1.multiplyMatrix(matrix2);

                    if (matrix3 == null){
                        System.out.println("Matrisene kan ikke multipliseres\n");
                    }
                    else{
                        System.out.printf("\nMatrise 1:\n%s\nMatrise 2:\n%s\nProdukt:\n%s\n", matrix1.displayMatrix(), matrix2.displayMatrix(),matrix3.displayMatrix());
                    }
                    break;
                case 3:
                    System.out.printf("\nMatrise:\n%s\nTransponert:\n%s\n", matrix1.displayMatrix(), matrix1.transposeMatix().displayMatrix());
                    break;
                default:
                    break;
            }
        }
    }

    public static Matrix chooseMatrix(ArrayList<Matrix> objects) {
        Scanner scannerObj = new Scanner(System.in);
        int choice;
        Matrix matrix;

        for (int i = 0; i < objects.size(); i++) {
            System.out.printf("[%d]\n.....\n", i+1);
            System.out.println(objects.get(i).displayMatrix());
        }
        System.out.printf("[%d] Lag en ny matrise\n", objects.size()+1);
        choice = scannerObj.nextInt();
        
        if (choice == objects.size()+1){
            matrix = Matrix.createMatrix();
            objects.add(matrix);
        }
        else{
            matrix = objects.get(choice-1);
        }

        return matrix;   
    }
}
