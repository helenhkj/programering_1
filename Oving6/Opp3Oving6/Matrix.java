package oving6.Opp3Oving6;

import java.util.Scanner;

/*
 * Klasse som representerer en matrise
 * @attribute matrix matrisen
 * @method createMatrix() lager en matrise
 * @method getMatrix() returnerer matrisen
 * @method displayMatrix() returnerer matrisen som en string
 * @method addMatrix(Matrix matrixObj) legger sammen to matriser
 * @method multiplyMatrix(Matrix matrixObj) multipliserer to matriser
 * @method transposeMatix() transponerer en matrise
 */
public class Matrix {
    // Instansattributter
    private int[][] matrix;

    /**
     * Konstruktør
     * @param matrix
     */
    public Matrix(int[][] matrix) {
        this.matrix = matrix;
    }

    /**
     * Metode som lager en matrise
     * @return
     */
    public static Matrix createMatrix(){
        Scanner scannerObj = new Scanner(System.in);
        int n, m;
        int[][] matrix;

        System.out.println("Lag en matrise:");
        System.out.println("Skriv inn antall rader:");
        n = scannerObj.nextInt();
        System.out.println("Skriv inn antall kolonner:");
        m = scannerObj.nextInt();
        matrix = new int[n][m];

        // Fyller matrisen med tall
        for (int i = 0; i < n; i++) {
            System.out.printf("Skriv inn %d tall for rad %d:\n", m, i+1);
            for (int j = 0; j < m; j++) {
                matrix[i][j] = scannerObj.nextInt();
            }
        }
        return new Matrix(matrix);
    }

    /**
     * Metode som returnerer matrisen
     * @return
     */
    public int[][] getMatrix() {
        return this.matrix;
    }

    /**
     * Metode som returnerer matrisen som en string
     * @return
     */
    public String displayMatrix(){
        String matrixString = "";
        for (int i = 0; i < this.matrix.length; i++) {
            matrixString += "[";
            for (int j = 0; j < this.matrix[0].length; j++) {
                matrixString += this.matrix[i][j];
                if (j != this.matrix[0].length-1){
                    matrixString += ", ";
                }
            }
            matrixString += "]\n";
        }
        return matrixString;
    }

    /**
     * Metode som legger sammen to matriser
     * @param matrixObj
     * @return
     */
    public Matrix addMatrix(Matrix matrixObj) {
        int[][] otherMatrix = matrixObj.getMatrix();
        int[][] newMatrix = new int[this.matrix.length][this.matrix[0].length];

        // Sjekker om matrisene er like store
        if (this.matrix.length == otherMatrix.length && this.matrix[0].length == otherMatrix[0].length) {
            // Legger sammen matrisene element for element
            for (int i = 0; i < this.matrix.length; i++) {
                for (int j = 0; j < this.matrix[0].length; j++) {
                    newMatrix[i][j] = this.matrix[i][j] + otherMatrix[i][j];
                }
            }
            return new Matrix(newMatrix);
        }
        else{
            return null;
        }
    }

    /**
     * Metode som multipliserer to matriser
     * @param matrixObj
     * @return
     */
    public Matrix multiplyMatrix(Matrix matrixObj){
        int[][] otherMatrix = matrixObj.getMatrix();
        int[][] newMatrix = new int[this.matrix.length][otherMatrix[0].length];

        // Sjekker om matrisene kan multipliseres
        if (this.matrix[0].length == otherMatrix.length){
            // Multipliserer matrisene
            for (int i = 0; i < this.matrix.length; i++) {
                for (int j = 0; j < otherMatrix[0].length; j++) {
                    for (int k = 0; k < this.matrix[0].length; k++) {
                        newMatrix[i][j] += this.matrix[i][k] * otherMatrix[k][j];
                    }
                }
            }
            return new Matrix(newMatrix);
        }
        else{
            return null;
        }
    }

    /**
     * Metode som transponerer en matrise
     * @return
     */
    public Matrix transposeMatix(){
        int column = this.matrix.length;
        int row = this.matrix[0].length;
        int[][] newMatrix = new int[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                newMatrix[i][j] = this.matrix[j][i];
            }
        }
        return new Matrix(newMatrix);
    }
}
