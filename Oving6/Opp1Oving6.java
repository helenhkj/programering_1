package oving6;

import java.util.Random;

public class Opp1Oving6 {
    public static void main(String[] args) {
        Random randomObj = new Random();
        int tall;
        int a = 10;
        int[] antall = new int[a];

        for (int i = 0; i < 1000; i++) {
            tall = randomObj.nextInt(a);
            for (int j = 0; j < antall.length ; j++) {
                if (tall == j){
                    antall[j] += 1;
                }
            }
        }
    
        for (int i = 0; i < antall.length; i++) {
            // Runder opp når desimalen er strørre eller lik 5, bruker modulus for å finne desimalen og sjekker om den er større eller lik 0.5, hvis den er det så legges 1 til, hvis ikke så legges 0 til
            int newNumber = antall[i]/10 + (((antall[i]/10.0) % 1.0 >= 0.5) ? 1 : 0);
            System.out.printf("%d : %4d : %s\n", i, antall[i], "*".repeat((newNumber)));
        }
    }
}
