import java.util.Scanner;

public class Oving9Opp1 {
    public static void main(String[] args) {
        AssignmentOverview assignmentOverview = new AssignmentOverview();
        int option;
        Scanner scannerObj = new Scanner(System.in);

        // meny
        while (true) {
            // hovedmeny
            System.out.println("\nStudenter:");
            for (int i = 0; i < assignmentOverview.getNumberOfStudents(); i++){
                System.out.printf("[%d] %s\n", i+1, assignmentOverview.getStudent(i).getName());
            }
            System.out.printf("[%d] Legg til student\n", assignmentOverview.getNumberOfStudents()+1);
            System.out.println("[0] Avslutt");

            option = scannerObj.nextInt();

            if (option == 0){ // avslutt
                break;
            } else if (option == assignmentOverview.getNumberOfStudents()+1){ // legg til student
                assignmentOverview.addStudent();
            } else if (option > 0 && option <= assignmentOverview.getNumberOfStudents()){ // studentmeny
                Student currentStudent = assignmentOverview.getStudent(option-1);
                while (true){
                    System.out.println(currentStudent.getName());
                    System.out.println("[1] Legg til godkjente oppgaver");
                    System.out.println("[2] Vis antall godkjente oppgaver");
                    System.out.println("[0] Tilbake");
                    option = scannerObj.nextInt();
                    if (option == 0){ // tilbake
                        break;
                    } else if (option == 1){ // legg til godkjente oppgaver
                        System.out.print("Antall godkjente oppgaver: ");
                        assignmentOverview.addAssignmentsToStudent(currentStudent, scannerObj.nextInt());
                    } else if (option == 2) { // vis antall godkjente oppgaver
                        System.out.printf("%d godkjente oppgaver\n",assignmentOverview.getNumberOfAssignments(currentStudent));
                    } else {
                        System.out.println("Ugyldig valg");
                    }
                }
            } else {
                System.out.println("Ugyldig valg");
            }
        }
    }
}
