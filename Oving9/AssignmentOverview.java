import java.util.ArrayList;

public class AssignmentOverview {
    // instansattributter
    private ArrayList<Student> students;
    private int numberOfStudents;

    /**
     * Konstruktør for klassen AssignmentOverview
     */
    public AssignmentOverview(){
        this.students = new ArrayList<Student>();
        this.numberOfStudents = 0;
    }

    /**
     * Returnerer antall studenter
     * @return Antall studenter
     */
    public int getNumberOfStudents(){
        return this.numberOfStudents;
    }

    /**
     * Returnerer en liste med alle studentene
     * @return Liste med alle studentene
     */
    public ArrayList<Student> getStudents(){
        return this.students;
    }

    /**
     * Returnerer antall godkjente oppgaver til en gitt student
     * @param student Studenten som antall godkjente oppgaver skal returneres for
     * @return Antall godkjente oppgaver til studenten
     */
    public int getNumberOfAssignments(Student student){
        return student.getNumberOfAssignments();
    }

    /**
     * Returnerer en gitt student
     * @param index Indeksen til studenten som skal returneres
     * @return Studenten som skal returneres
     */
    public Student getStudent(int index){
        return this.students.get(index);
    }

    /**
     * Legger til en ny student i listen over studenter
     */
    public void addStudent(){
        Student student;
        boolean duplicate;
        while (true) {
            duplicate = false;
            student = Student.createStudent();
            for (Student s : this.students) {
                if (s.getName().equals(student.getName())) { // lage egne equals
                    System.out.println("Studenten finnes allerede");
                    duplicate = true;
                    break;
                }
            }
            if (!duplicate){
                break;
            }
        }
        this.students.add(student);
        this.numberOfStudents++;
    }

    /** Legger til et gitt antall godkjente oppgaver til en gitt student
     * @param student Studenten som skal få godkjent oppgaver
     * @param numberOfAssignments Antall godkjente oppgaver som skal legges til
     */
    public void addAssignmentsToStudent(Student student, int numberOfAssignments){
        student.addToNumberOfAssignments(numberOfAssignments);
    }

    /**
     * Returnerer en string med informasjon om alle studentene
     * @return String med informasjon om alle studentene
     */
    public String toString(){
        String output = "";
        for (int i = 0; i < this.numberOfStudents; i++){
            output += this.students.get(i).toString() + "\n";
        }
        return output;
    }
}
