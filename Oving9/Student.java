import java.util.Scanner;

public class Student {
    // instansattributter
    private String name;
    private int numberOfAssignments;

    /** Konstruktør for Student-objekt
     * @param name Navnet til studenten
     * @param numberOfAssignments Antall godkjente oppgaver
     */
    public Student(String name, int numberOfAssignments){
        this.name = name;
        this.numberOfAssignments = numberOfAssignments;
    }

    /**
     * Oppretter et nytt Student-objekt
     * @return Det nye Student-objektet
     */
    public static Student createStudent(){ // lage object i main, ikke i klassen, mer som et menysystem
        Scanner scannerObj = new Scanner(System.in);
        String name;
        int numberOfAssignments;

        System.out.print("Navn: ");
        name = scannerObj.nextLine();
        System.out.print("Antall godkjente oppgaver: ");
        numberOfAssignments = scannerObj.nextInt();

        return new Student(name, numberOfAssignments);
    }

    /**
     * Returnerer navnet til studenten
     * @return Navnet til studenten
     */
    public String getName(){
        return this.name;
    }

    /**
     * Returnerer antall godkjente oppgaver
     * @return Antall godkjente oppgaver
     */
    public int getNumberOfAssignments(){
        return this.numberOfAssignments;
    }

    /**
     * Legger til et gitt antall godkjente oppgaver
     * @param numberOfAssignments Antall godkjente oppgaver som skal legges til
     */
    public void addToNumberOfAssignments(int numberOfAssignments){
        this.numberOfAssignments += numberOfAssignments;
    }

    /**
     * Returnerer en string med informasjon om studenten
     * @return String med informasjon om studenten
     */
    @Override
    public String toString(){
        return "Navn: " + this.name + "\nAntall godkjente oppgaver: " + this.numberOfAssignments + "\n";
    }
}
