package Opp2;

public class Course {
    private final String name;
    private final String type;
    private final int price;
    private final String recipe;

    public Course(String name, String type, int price, String recipe){
        this.name = name;
        this.type = type;
        this.price = price;
        this.recipe = recipe;
    }

    public String getName(){
        return this.name;
    }

    public String getType(){
        return this.type;
    }

    public int getPrice(){
        return this.price;
    }

    public String getRecipe(){
        return this.recipe;
    }

    @Override
    public String toString(){
        return this.getName() + " | " + this.getType() + " | " + this.getPrice() + "kr";
    }
}
