package Opp2;
import java.util.Scanner;
import java.util.ArrayList;

public class Oving10Opp2 {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        int choice;
        boolean done = false;
        int option;
        Menu currentMenu;
        Course currentCourse;
        ArrayList<Course> testCourses = new ArrayList<Course>();
        ArrayList<Menu> testMenus = new ArrayList<Menu>();
        testCourses.add(new Course("Test1", "Forrett", 100, "..."));
        testCourses.add(new Course("Test2", "Hovedrett", 200, "..."));
        testCourses.add(new Course("Test3", "Dessert", 50, "..."));
        testCourses.add(new Course("Test4", "Forrett", 100, "..."));
        testMenus.add(new Menu("TestMenu1"));
        testMenus.add(new Menu("TestMenu2"));
        testMenus.get(0).addCourse(testCourses.get(0));
        testMenus.get(0).addCourse(testCourses.get(1));
        testMenus.get(1).addCourse(testCourses.get(2));
        testMenus.get(1).addCourse(testCourses.get(3));
        MenuRegister menuRegister = new MenuRegister(testMenus, testCourses);

        while (!done){
            System.out.println("\n[1] Legg til rett");
            System.out.println("[2] Legg til meny");
            System.out.println("[3] Vis retter");
            System.out.println("[4] Vis menyer");
            System.out.println("[5] Finn rett gitt navn");
            System.out.println("[6] Vis retter av gitt type");
            System.out.println("[7] Vis menyer innenfor gitt prisintervall");
            System.out.println("[0] Avslutt");
            choice = scannerObj.nextInt();
            scannerObj.nextLine();

            switch (choice){
                case 1:
                    menuRegister.addCourse();
                    break;
                case 2:
                    menuRegister.addMenu();
                    break;
                case 3:
                    while (true){
                        System.out.println("\nRetter:");
                        menuRegister.displayCourses(menuRegister.getCourses());
                        System.out.println("[0] Tilbake");
                        option = scannerObj.nextInt();
                        scannerObj.nextLine();
                        if (option == 0){
                            break;
                        }
                        else if (option > 0 && option <= menuRegister.getCourses().size()){
                            System.out.println(menuRegister.getCourse(option - 1));
                        }
                        else {
                            System.out.println("Ugyldig valg");
                        }
                    }
                    break;
                case 4:
                    menuRegister.displayMenus(menuRegister.getMenus());
                    option = scannerObj.nextInt();
                    scannerObj.nextLine();
                    currentMenu = menuRegister.getMenu(option - 1);
                    while (true){
                        System.out.println(currentMenu);
                        System.out.println("[1] Legg til rett");
                        System.out.println("[0] Tilbake");
                        choice = scannerObj.nextInt();
                        scannerObj.nextLine();
                        if (choice == 1){
                            menuRegister.displayCourses(menuRegister.getCourses());
                            System.out.print("Velg rett: ");
                            currentCourse = menuRegister.getCourse(scannerObj.nextInt() - 1);
                            scannerObj.nextLine();
                            if (menuRegister.getCourseByNameInMenu(currentCourse.getName(), currentMenu) != null){
                                System.out.println("Rett med dette navnet finnes allerede i menyen");
                            }
                            else {
                                currentMenu.addCourse(currentCourse);
                            }
                        }
                        else if (choice == 0){
                            break;
                        }
                        else {
                            System.out.println("Ugyldig valg");
                        }
                    }
                    break;
                case 5:
                    System.out.print("Navn: ");
                    System.out.println(menuRegister.getCourseByName(scannerObj.nextLine()));
                    break;
                case 6:
                    System.out.print("Type: ");
                    menuRegister.displayCourses(menuRegister.getCoursesOfType(scannerObj.nextLine()));
                    break;
                case 7:
                    System.out.print("Minimum pris: ");
                    int minPrice = scannerObj.nextInt();
                    System.out.print("Maksimum pris: ");
                    int maxPrice = scannerObj.nextInt();
                    scannerObj.nextLine();
                    menuRegister.displayMenus(menuRegister.getMenusInPriceRange(minPrice, maxPrice));
                    break;
                case 0:
                    done = true;
                    break;
                default:
                    System.out.println("Ugyldig valg");
                    break;
            }
        }
    }
}
