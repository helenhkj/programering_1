package Opp2;

import java.util.ArrayList;
import java.util.Scanner;

public class MenuRegister {
    private ArrayList<Menu> menus;
    private ArrayList<Course> courses;

    public MenuRegister(){
        this.menus = new ArrayList<>();
        this.courses = new ArrayList<>();
    }

    public MenuRegister(ArrayList<Menu> menus, ArrayList<Course> courses){ // for testing
        this.menus = menus;
        this.courses = courses;
    }

    public ArrayList<Menu> getMenus(){
        return this.menus;
    }

    public ArrayList<Course> getCourses(){
        return this.courses;
    }

    public void addMenu(){
        Scanner scannerObj = new Scanner(System.in);
        String name;
        int choice;
        boolean done = false;
        Course course;

        System.out.print("Navn: ");
        name = scannerObj.nextLine();

        Menu menu = new Menu(name);

        while (!done){
            System.out.println("[1] Legg til rett");
            System.out.println("[0] Tilbake/Ferdig");
            choice = scannerObj.nextInt();
            scannerObj.nextLine();

            switch (choice){
                case 1:
                    // TODO: check if course is already in menu
                    this.displayCourses(this.getCourses());
                    System.out.print("Velg rett: ");
                    course = this.getCourse(scannerObj.nextInt() - 1);
                    if (this.getCourseByNameInMenu(course.getName(), menu) != null){
                        System.out.println("Rett med dette navnet finnes allerede i menyen");
                        break;
                    }
                    menu.addCourse(course);
                    scannerObj.nextLine();
                    break;
                case 0:
                    done = true;
                    break;
                default:
                    System.out.println("Ugyldig valg");
                    break;
            }
        }

        this.menus.add(menu);
    }

    public void addCourse(){
        Scanner scannerObj = new Scanner(System.in);
        String name;
        String type;
        int price;
        String recipe;

        while (true){
            System.out.print("Navn: ");
            name = scannerObj.nextLine();
            System.out.print("Type: ");
            type = scannerObj.nextLine();
            System.out.print("Pris: ");
            price = scannerObj.nextInt();
            scannerObj.nextLine();
            System.out.print("Oppskrift: ");
            recipe = scannerObj.nextLine();

            if (this.getCourseByName(name) != null){
                System.out.println("Rett med dette navnet finnes allerede");
            }
            else {
                break;
            }
        }

        this.courses.add(new Course(name, type, price, recipe));
    }

    public Menu getMenu(int index){
        return this.getMenus().get(index);
    }

    public Course getCourse(int index){
        return this.getCourses().get(index);
    }

    public Course getCourseByName(String name){
        for (Course course : this.getCourses()){
            if (course.getName().equals(name)){
                return course;
            }
        }
        return null;
    }

    public ArrayList<Course> getCoursesOfType(String type){
        ArrayList<Course> coursesOfType = new ArrayList<>();
        for (Course course : this.getCourses()){
            if (course.getType().equals(type)){
                coursesOfType.add(course);
            }
        }
        return coursesOfType;
    }

    public ArrayList<Menu> getMenusInPriceRange(int minPrice, int maxPrice){
        ArrayList<Menu> menusInPriceRange = new ArrayList<>();
        for (Menu menu : this.getMenus()){
            if (menu.getTotalPrice() >= minPrice && menu.getTotalPrice() <= maxPrice){
                menusInPriceRange.add(menu);
            }
        }
        return menusInPriceRange;
    }

    public Course getCourseByNameInMenu(String name, Menu menu){
        for (Course course : menu.getCourses()){
            if (course.getName().equals(name)){
                return course;
            }
        }
        return null;
    }

    public void displayMenus(ArrayList<Menu> menus){
        for (int i = 0; i < menus.size(); i++) {
            System.out.printf("[%d] %s\n", i + 1, menus.get(i).getName());
        }
    }

    public void displayCourses(ArrayList<Course> courses){
        for (int i = 0; i < courses.size(); i++) {
            System.out.printf("[%d] %s\n", i + 1, courses.get(i).getName());
        }
    }
}
