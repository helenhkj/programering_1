package Opp2;

import java.util.ArrayList;

public class Menu {
    private final String name;
    private ArrayList<Course> courses;

    public Menu(String name){
        this.name = name;
        this.courses = new ArrayList<>();
    }

    public ArrayList<Course> getCourses(){
        return this.courses;
    }

    public String getName(){
        return this.name;
    }

    public int getTotalPrice(){
        int price = 0;
        for (Course course : this.getCourses()){
            price += course.getPrice();
        }
        return price;
    }

    public void addCourse(Course course){
        this.courses.add(course);
    }

    @Override
    public String toString(){
        String str = this.getName() + "\n";
        for (Course course : this.getCourses()){
            str += course.toString() + "\n";
        }
        str += "Totalpris: " + this.getTotalPrice() + "kr";
        return str;
    }
}
