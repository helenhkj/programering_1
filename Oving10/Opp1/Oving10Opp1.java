package Opp1;

import java.util.ArrayList;
import java.util.Scanner;

public class Oving10Opp1 {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        ArrayList<Event> testEvents = new ArrayList<Event>();
        testEvents.add(new Event(1, "Test1", "Oslo", "NTNU", "Konferanse", "202001010000"));
        testEvents.add(new Event(2, "Test2", "Trondheim", "NTNU", "Konferanse", "202001020000"));
        testEvents.add(new Event(3, "Test3", "Oslo", "NTNU", "Foredrag", "202001030000"));
        testEvents.add(new Event(4, "Test4", "Trondheim", "NTNU", "Foredrag", "202001040000"));
        testEvents.add(new Event(5, "Test5", "Oslo", "NTNU", "Fest", "202001051400"));
        testEvents.add(new Event(6, "Test6", "Trondheim", "NTNU", "Fest", "202001060000"));
        testEvents.add(new Event(7, "Test7", "Oslo", "NTNU", "Fest", "202001051100"));

        EventRegister eventRegister = new EventRegister(testEvents);
        System.out.println(eventRegister.getSortedEvents());

        int choice;

        //meny
        while (true){
            System.out.println("[1] Legg til nytt arrangement");
            System.out.println("[2] Vis arrangementer ved git sted");
            System.out.println("[3] Vis arrangementer ved git dato");
            System.out.println("[4] Vis arrangementer innenfor git tidsrom");
            System.out.println("[5] Vis arrangementer");
            System.out.println("[0] Avslutt");
            choice = scannerObj.nextInt();
            if (choice == 1){
                eventRegister.addEvent();
            }
            else if (choice == 2){
                System.out.print("Sted: ");
                scannerObj.nextLine();
                System.out.println(eventRegister.getEventsByPlace(scannerObj.nextLine()));
            }
            else if (choice == 3){
                System.out.print("Dato (yyyymmdd): ");
                scannerObj.nextLine();
                System.out.println(eventRegister.getEventsByDate(scannerObj.nextInt()));
            }
            else if (choice == 4){
                System.out.print("Startdato (yyyymmdd): ");
                scannerObj.nextLine();
                int startDate = scannerObj.nextInt();
                System.out.print("Sluttdato (yyyymmdd): ");
                scannerObj.nextLine();
                int endDate = scannerObj.nextInt();
                System.out.println(eventRegister.getEventsInInterval(startDate, endDate));
            }
            else if (choice == 5){
                System.out.println(eventRegister.getSortedEvents());
            }
            else if (choice == 0){
                break;
            }
            else {
                System.out.println("Ugyldig valg");
            }
        }
    }
}
