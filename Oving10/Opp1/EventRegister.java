package Opp1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class EventRegister implements Comparator<Event>{
    private ArrayList<Event> events;

    public EventRegister(){
        this.events = new ArrayList<Event>();
    }

    public EventRegister(ArrayList<Event> events){ // for testing
        this.events = events;
    }

    public void addEvent(){
        Scanner scannerObj = new Scanner(System.in);
        int number;
        String name;
        String place;
        String organiser;
        String type;
        String date;

        System.out.print("Nummer: ");
        number = scannerObj.nextInt();
        scannerObj.nextLine();
        System.out.print("Navn: ");
        name = scannerObj.nextLine();
        System.out.print("Sted: ");
        place = scannerObj.nextLine();
        System.out.print("Arrangør: ");
        organiser = scannerObj.nextLine();
        System.out.print("Type: ");
        type = scannerObj.nextLine();
        System.out.print("Dato(yyyymmddHHMM): ");
        date = scannerObj.nextLine();

        this.events.add(new Event(number, name, place, organiser, type, date));
    }

    public ArrayList<Event> getEvents(){
        return this.events;
    }

    public Event getEvent(int index){
        return this.events.get(index);
    }

    public String getEventsByPlace(String place){
        ArrayList<Event> eventsByPlace = new ArrayList<Event>();
        for (Event event : this.getEvents()){
            if (event.getPlace().equals(place)){
                eventsByPlace.add(event);
            }
        }

        return this.displayEvents(eventsByPlace.toArray(new Event[eventsByPlace.size()]));
    }

    public String getEventsByDate(int date){
        ArrayList<Event> eventsByDate = new ArrayList<Event>();
        for (Event event : this.getEvents()){
            int eventDate = Integer.parseInt(event.getDateWithoutTime()); // bruk kun dato, ikke klokkeslett (yyyymmdd)
            if (eventDate == date){
                eventsByDate.add(event);
            }
        }

        return this.displayEvents(eventsByDate.toArray(new Event[eventsByDate.size()]));
    }

    public String getEventsInInterval(int startDate, int endDate){
        ArrayList<Event> eventsInInterval = new ArrayList<Event>();
        Event[] sortedEvents = this.getEvents().toArray(new Event[this.getEvents().size()]);
        // Comparator<Event> compareDate = Comparator.comparing(Event::getDate);
        Collections.sort(Arrays.asList(sortedEvents), this);

        for (Event event : sortedEvents){
            int eventDate = Integer.parseInt(event.getDateWithoutTime()); // bruk kun dato, ikke klokkeslett (yyyymmdd)
            if (eventDate >= startDate && eventDate <= endDate){
                eventsInInterval.add(event);
            }
        }

        return this.displayEvents(eventsInInterval.toArray(new Event[eventsInInterval.size()]));
    }

    public String getSortedEvents(){
        Event[] sortedEvents = this.events.toArray(new Event[this.getEvents().size()]);
        Arrays.sort(sortedEvents);

        return this.displayEvents(sortedEvents);
    }

    private String displayEvents(Event[] events){
        String output = "";
        output += String.format("%-10s %-20s %-20s %-20s %-20s %-20s\n", "Nummer", "Navn", "Sted", "Arrangør", "Type", "Dato");
        for (Event event : events){
            output += event.toString() + "\n";
        }
        return output;
    }

    /**
     * Compares two events by date.
     * @param event1 The first event.
     * @param event2 The second event.
     * @return The result of the comparison.
     */
    @Override
    public int compare(Event event1, Event event2){
        // burde denne metoden være i Event-klassen?
        return event1.getDate().compareTo(event2.getDate());
    }
}
