package Opp1;

import java.lang.Comparable;

public class Event implements Comparable<Event> {
    private int number;
    private String name;
    private String place;
    private String organiser;
    private String type;
    private String date;

    public Event(int number, String name, String place, String organiser, String type, String date){
        this.number = number;
        this.name = name;
        this.place = place;
        this.organiser = organiser;
        this.type = type;
        this.date = date;
    }

    public int getNumber(){
        return this.number;
    }

    public String getName(){
        return this.name;
    }

    public String getPlace(){
        return this.place;
    }

    public String getOrganiser(){
        return this.organiser;
    }

    public String getType(){
        return this.type;
    }

    public String getDate(){
        return this.date;
    }

    public String getDateWithoutTime(){
        return this.date.substring(0, 8);
    }

    @Override
    public int compareTo(Event event){
        if (this.getPlace().compareTo(event.getPlace()) != 0){ // hvis sted ikke er likt, sammenlign sted
            return this.getPlace().compareTo(event.getPlace());
        }
        else if (this.getType().compareTo(event.getType()) != 0){ // his type ikke er lik, sammenlign type
            return this.getType().compareTo(event.getType());
        }
        else {
            return this.getDate().compareTo(event.getDate()); // hvis sted og type er likt, sammenlign dato
        }
    }

    @Override
    public String toString(){
        String date = "";
        if (this.date.length() < 12){
            date = "N/A";
        }
        else {
            date += "kl. " + this.date.substring(8, 10) + ":" + this.date.substring(10, 12) + " ";
            date += this.date.substring(6, 8) + "/" + this.date.substring(4, 6) + "-" + this.date.substring(0, 4);
        }
        return String.format("%-10d %-20s %-20s %-20s %-20s %-20s", this.number, this.name, this.place, this.organiser, this.type, date);
    }
}
