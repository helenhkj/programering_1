// Oppgave 1
// Skriv et program som regner om fra tommer til centimeter. En tomme er 2,54 centimeter. Sett opp testdatasett og prøv ut programmet.

import java.util.Scanner;

class Opp1_oving1{
    public static void main(String[] args){
        Scanner scannerObj = new Scanner(System.in);

        System.out.println("Lengde (tommer):");
        double tommer = scannerObj.nextDouble();

        double centimeter = tommer * 2.54;
        System.out.println(tommer + " tommer er " + centimeter + " cm");
    }
}