// Oppgave 3
// Skriv et program som leser inn et antall sekunder og beregner hvor mange timer, minutter og sekunder dette er. (Hint: Bruk heltallsdivisjon.) Sett opp testdatasett og prøv ut programmet.

import java.util.Scanner;

class Opp3_oving1 {
    public static void main(String[] args){
        Scanner scannerObj = new Scanner(System.in);

        System.out.println("Antall sekunder:");
        int sekunder = scannerObj.nextInt();

        int timer = (sekunder - sekunder%3600)/3600;
        sekunder = sekunder%120;
        int minutter = (sekunder - sekunder%60)/60;
        sekunder = sekunder%60;

        System.out.println(timer + " timer " + minutter + " minutter " + sekunder + " sekunder");
    }
}
