// Oppgave 2
// Skriv et program som regner om timer, minutter og sekunder til totalt antall sekunder. Sett opp testdatasett og prøv ut programmet.

import java.util.Scanner;

class Opp2_oving1 {
    public static void main(String[] args){
        Scanner scannerObj = new Scanner(System.in);

        System.out.println("Antall timer:");
        int timer = scannerObj.nextInt();
        System.out.println("Antall minutter:");
        int minutter = scannerObj.nextInt();
        System.out.println("Antall sekunder:");
        int sekunder = scannerObj.nextInt();

        sekunder = timer*(60*60) + minutter*60 + sekunder;
        System.out.println(sekunder);

    }
}
