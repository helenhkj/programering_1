// Oppgave 1
// Et år er skuddår dersom det er delelig med 4. Unntaket er hundreårene, de må være delelig med 400.
// Tegn aktivitetsdiagram som viser algoritmen for å finne ut om et år er skuddår. Årstallet skal leses inn fra brukeren. Sett opp testdata. Lag og prøv ut programmet.

package oving_2;

import java.util.Scanner;

public class Opp1_oving2 {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Årstall:");
        int year = scannerObj.nextInt();
        if (leap_year(year)){
            System.out.println(year + " er et skuddår.");
        }
        else {
            System.out.println(year + " er ikke et skuddår.");
        }
    }

    private static boolean leap_year(int year) {
        if (year%100 == 0){
            if (year%400 == 0){
                return true;
            }
            else {
                return false;
            }
        }
        else if (year%4 == 0){
            return true;
        }
        else {
            return false;
        }
    }
}