// Oppgave 2
// Lag et program som hjelper oss i forhold til følgende problemstilling: Kjøttdeig av merke A koster kr 35,90 for 450 gram, mens kjøttdeig av merke B koster kr 39,50 for 500 gram. Hvilket merke er billigst?

package oving_2;

public class Opp2_oving2 {
    public static void main(String[] args) {
        double a = 35.9/450;
        double b = 29.5/500;

        if (b < a){
            System.out.println("Kjøttdeig av merke B er billigere enn A");
        }
        else if (a == b){
            System.out.println("Kjøttdeig av merke A koster det samme som kjøttdeig av merke B");
        }
        else {
            System.out.println("Kjøttdeig av merke A er billigere enn B");
        }
    }
}
