package oving_5.opp1_oving5;

import java.util.Scanner;

public class Opp1_oving5 {
    /**
     * Main metode
     * @param args
     */
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        int option;
        Fraction.create_object();
        boolean run = true;

        // Kjører programmet til brukeren velger å avslutte
        while (run){
            System.out.println("\nVelg en brøk/heltall");
            for (int i = 0; i < Fraction.objects.size(); i++) {
                System.out.printf("[%d] %s\n", i+1, Fraction.objects.get(i).get_fraction());
            }
            System.out.printf("[%d] Legg til en brøk/heltall\n", Fraction.objects.size()+1);
            System.out.printf("[%d] Avslutt\n", Fraction.objects.size()+2);

            option = scannerObj.nextInt();
            
            // Legg til en ny brøk
            if (option == Fraction.objects.size()+1){
                Fraction.create_object();
            }
            // Avslutt
            else if (option == Fraction.objects.size()+2){
                run = false;
            }
            // Velg en brøk
            else{
                for (int i = 0; i < Fraction.objects.size(); i++) {
                    if (option == i+1){
                        run = Fraction.objects.get(i).run();
                    }
                }
            }
        }
    }
}


