package oving_5.opp1_oving5;

import java.util.Scanner;
import java.util.ArrayList;

public class Fraction {
    public static ArrayList<Fraction> objects = new ArrayList<Fraction>();
    private int numerator;
    private int denominator;

    /**
     * Konstruktør
     * @param numerator
     * @param denominator
     * @throws IllegalArgumentException
     */
    public Fraction(int numerator, int denominator) throws IllegalArgumentException{
        if (denominator == 0 ){
            throw new IllegalArgumentException("0 er ikke gydlig nevner");
        }
        else{
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    /**
     * Konstruktør
     * @param numerator
     */
    public Fraction(int numerator){
        this.numerator = numerator;
        this.denominator = 1;
    }

    /**
     * Lag et nytt Fraction objekt og legg det til i listen av objekter.
     * klasse metode
     */
    public static void create_object(){
        Scanner scannerObj = new Scanner(System.in);
        int option;
        while (true){
            System.out.println("[1] Skriv inn en brøk \n[2] Skriv inn et heltall");
            option = scannerObj.nextInt();
                switch (option) {
                    case 1:
                        System.out.println("Skriv in teller");
                        int numerator = scannerObj.nextInt();
                        System.out.println("Skriv in nevner");
                        int denominator = scannerObj.nextInt();
                        objects.add(new Fraction(numerator, denominator));
                        return;
                    case 2:
                        System.out.println("Skriv in et heltall");
                        int number = scannerObj.nextInt();
                        objects.add(new Fraction(number));
                        return;
                }
            }
    }

    /**
     * Hent teller
     * @return
     */
    public int get_numerator(){
        return this.numerator;
    }

    /**
     * Hent nevner
     * @return
     */
    public int get_denominator(){
        return this.denominator;
    }

    /**
     * Hent brøk som en string
     * @return
     */
    public String get_fraction(){
        if (this.denominator == 1){
            return String.format("%d", this.numerator);
        }
        else{
            return String.format("%d / %d", this.numerator, this.denominator);
        }
    }

    /*
     * Legg til en brøk til denne brøken
     * @param other_fraction
     */
    public void sum(Fraction other_fraction){
        int new_numerator = this.numerator * other_fraction.get_denominator() + other_fraction.get_numerator() * this.denominator;
        int new_denominator = this.denominator * other_fraction.get_denominator();
        this.numerator = new_numerator;
        this.denominator  = new_denominator;
        this.reduce();
    }

    /**
     * Subtraher en brøk fra denne brøken
     * @param other_fraction
     */
    public void difference(Fraction other_fraction) {
        int new_numerator = this.numerator * other_fraction.get_denominator() - other_fraction.get_numerator() * this.denominator;
        int new_denominator = this.denominator * other_fraction.get_denominator();
        this.numerator = new_numerator;
        this.denominator  = new_denominator;
        this.reduce();
    }

    /**
     * Multipliser en brøk med denne brøken
     * @param other_fraction
     */
    public void product(Fraction other_fraction) {
        this.numerator = this.numerator * other_fraction.get_numerator();
        this.denominator  = this.denominator * other_fraction.get_denominator();
        this.reduce();
    }

    /**
     * Divider denne brøken med en annen brøk
     * @param other_fraction
     */
    public void quotient(Fraction other_fraction) {
        this.numerator = this.numerator * other_fraction.get_denominator();
        this.denominator  = this.denominator * other_fraction.get_numerator();
        this.reduce();
    }

    /**
     * Forkort brøken
     */
    private void reduce(){
        // finner største felles divisor og forkorter brøken
        int gcd = greatest_comon_divisor(this.numerator, this.denominator);
        this.numerator = this.numerator / gcd;
        this.denominator = this.denominator / gcd;

        // endre fortegn hvis nevner er negativ
        if (this.denominator < 0){
            this.numerator = -this.numerator;
            this.denominator = -this.denominator;
        }
    }

    /**
     * Største felles divisor ved bruk av Euklids algoritme
     * 
     * @param a
     * @param b
     * @return
     */
    private int greatest_comon_divisor(int a, int b){
        if (b == 0){
            return a;
        }
        else{
            return greatest_comon_divisor(b, a % b);
        }
    }

    /**
     * Kjør programmet
     * @return
     */
    public boolean run() {
        Scanner scannerObj = new Scanner(System.in);
        int option_operation;
        int option_fraction;
        Fraction other_fraction;

        // Kjører programmet til brukeren velger å avslutte
        while (true){
            String [] operations = {"Sumer", "Subtrakter", "Multipilser", "Divider"};
            System.out.println("\nVelg en operasjon");
            for (int i = 0; i < operations.length; i++) {
                System.out.printf("[%d] %s\n", i+1, operations[i]);
            }
            option_operation = scannerObj.nextInt();
    
            System.out.println("\nVelg en brøk/heltall");
            for (int i = 0; i < Fraction.objects.size(); i++) {
                if (Fraction.objects.get(i) != this){
                    System.out.printf("[%d] %s\n", i+1, Fraction.objects.get(i).get_fraction());
                }
            }
            System.out.printf("[%d] Legg til en brøk/heltall\n", Fraction.objects.size()+1);
            System.out.printf("[%d] Avslutt\n", Fraction.objects.size()+2);

            option_fraction = scannerObj.nextInt();

            // Legg til en ny brøk
            if (option_fraction == Fraction.objects.size()+1){
                Fraction.create_object();
                other_fraction = Fraction.objects.get(Fraction.objects.size()-1);
            }
            // Avslutt
            else if (option_fraction == Fraction.objects.size()+2){
                return false;
            }
            // Velg en brøk
            else{
                other_fraction = Fraction.objects.get(option_fraction-1);
            }

            // Utfør valgt operasjon
            switch (option_operation) {
                case 1:     // summer
                    System.out.printf("\n%s + %s = ", this.get_fraction(), other_fraction.get_fraction());
                    this.sum(other_fraction);
                    System.out.println(this.get_fraction());
                    break;
                case 2:     // subtraher
                    System.out.printf("\n%s - %s = ", this.get_fraction(), other_fraction.get_fraction());
                    this.difference(other_fraction);
                    System.out.println(this.get_fraction());
                    break;
                case 3:     // multipliser
                    System.out.printf("\n%s * %s = ", this.get_fraction(), other_fraction.get_fraction());
                    this.product(other_fraction);
                    System.out.println(this.get_fraction());
                    break;
                case 4:     // divider
                    System.out.printf("\n%s / %s = ", this.get_fraction(), other_fraction.get_fraction());
                    this.quotient(other_fraction);
                    System.out.println(this.get_fraction());
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
