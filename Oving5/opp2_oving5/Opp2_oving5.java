package oving_5.opp2_oving5;

public class Opp2_oving5 {
    public static void main(String[] args) {
        MinRandom random = new MinRandom();
        int a = -10;
        int b = 20;

        // Tester nesteHeltall
        System.out.printf("Tester nesteHeltall med nedre grense = %d og øvre grense = %d\n", a, b);
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nesteHeltall(a, b));
        }

        // Tester nesteDesimaltall
        System.out.printf("Tester nesteDesimaltall med nedre grense = %d og øvre grense = %d\n", a, b);
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nesteDesimaltall(a, b));
        }
    }
}
