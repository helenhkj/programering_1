package oving_5.opp2_oving5;

import java.util.Random;

public class MinRandom {
    Random randomObj = new Random();
    

    /*
     * Returnerer et tilfeldig heltall tall innenfor et intervall
     */
    public int nesteHeltall(int nedre, int ovre)  {
        if (nedre > ovre){
            int a = nedre;
            nedre = ovre;
            ovre = a;
        }
        else if (nedre == ovre){
            return nedre;
        }
        ovre += 1;
        // randomObj.nextInt(ovre-nedre) + nedre = [0, ovre-nedre> + nedre = [nedre, ovre>
        return randomObj.nextInt(ovre-nedre) + nedre;
    }

    /*
     * Returnerer et tilfeldig desimaltall tall innenfor et intervall
     */
    public double nesteDesimaltall(double nedre, double ovre){
        if (nedre > ovre){
            double a = nedre;
            nedre = ovre;
            ovre = a;
        }
        else if (nedre == ovre){
            return nedre;
        }
        // (ovre-nedre)*randomObj.nextDouble() + nedre = [0.0, ovre-nedre> + nedre = [nedre, ovre>
        return randomObj.nextDouble()*(ovre-nedre) + nedre;
    }
}
