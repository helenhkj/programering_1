package oving7.Opp1Oving7;

public class NewString {
    private String string;

    /**
     * Konstruktør
     * @param string
     */
    public NewString(String string) {
        this.string = string;
    }

    /**
     * Metode som returnerer string
     * @return
     */
    public String getString() {
        return string;
    }

    /**
     * Metode som forkorter string
     * @return
     */
    public String abbreviate(){
        String[] words = this.string.split(" ");
        String newString = "";
        for (String word : words) {
            newString += word.charAt(0);
        }
        return newString;
    }

    /**
     * Metode som fjerner symbol fra string
     * @param symbol
     * @return
     */
    public String removeSymbol(char symbol){
        String newString = "";
        // this.string = this.string.toLowerCase();
        for (int i = 0; i < this.string.length(); i++) {
            if (this.string.charAt(i) != symbol){
                newString += this.string.charAt(i);
            }
        }
        return newString;
    }
}
