package oving7.Opp2Oving7;

public class TextProcessing {
    private String text;

    /**
     * Konstruktør
     * @param text
     */
    public TextProcessing(String text) {
        this.text = text;
    }

    /**
     * Metode som returnerer antall ord i text
     * @return
     */
    public Integer getWordCount(){
        return this.text.split(" ").length;
    }

    /**
     * Metode som returnerer gjennomsnittlig ordlengde i text
     * @return
     */
    public Double getaverageWordLength(){
        String[] words = this.text.split(" ");
        Integer totalLength = 0;
        
        for (String word : words) {
            for (String letter : words) {
                if (letter.equals(".") || letter.equals("!") || letter.equals("?") || letter.equals(":")){
                    word = word.replace(letter, "");
                }
            }
            totalLength += word.length();
        }
        return (double) totalLength / words.length;
    }

    /**
     * Metode som returnerer gjennomsnittlig ord per setning i text
     * @return
     */
    public Double getaverageWordsPerSentence(){
        String[] sentences = this.text.split("\\.|\\!|\\?|\\:");
        Integer totalWords = 0;
        for (String sentence : sentences) {
            totalWords += sentence.split(" ").length;
        }
        return (double) totalWords / sentences.length;
    }

    /**
     * Metode som erstatter oldWord med newWord i text
     * @param oldWord
     * @param newWord
     * @return
     */
    public String replaceWord(String oldWord, String newWord){
        String newText = "";
        
        oldWord = oldWord.toLowerCase();
        oldWord = "\\b" + oldWord + "\\b";
        newText = this.text.replaceAll(oldWord, newWord);

        return newText;
    }

    /**
     * Metode som returnerer text
     * @return
     */
    public String getText() {
        return this.text;
    }

    /**
     * Metode som returnerer text i uppercase
     * @return
     */
    public String getUpperCase(){
        return this.text.toUpperCase();
    }
}
