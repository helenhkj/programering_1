package oving7.Opp2Oving7;

import java.util.Scanner;

public class Opp2Oving7 {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);
        TextProcessing text;
        String [] operations = {"Antall ord", "Gjennomsnittlig ord lengde", "Gjenomsnittlig ord per peride", "Erstatt ord", "Skriv ut tekst", "Skriv ut tekt i uppercase"};

        System.out.println("Skriv inn en tekst:");
        text = new TextProcessing(scannerObj.nextLine());

        while (true){
            System.out.println("\nVelg en operasjon:");
            for (int i = 0; i < operations.length; i++) {
                System.out.printf("[%d] %s\n", i+1, operations[i]);
            }
            System.out.printf("[%d] ny tekst\n", operations.length+1);
            System.out.printf("[%d] avslutt\n", operations.length+2);

            int choice = scannerObj.nextInt();
            scannerObj.nextLine();

            if (choice == operations.length+1){
                System.out.println("Skriv inn en tekst:");
                text = new TextProcessing(scannerObj.nextLine());
            }
            else if (choice == operations.length+2){
                break;
            }
            else if (choice >= 1 && choice <= operations.length){
                switch (choice){
                    case 1:
                        System.out.printf("Antall ord: %d\n", text.getWordCount());
                        break;
                    case 2:
                        System.out.printf("Gjennomsnittlig ordlengde: %.2f\n", text.getaverageWordLength());
                        break;
                    case 3:
                        System.out.printf("Gjennomsnittlig ord per setning: %.2f\n", text.getaverageWordsPerSentence());
                        break;
                    case 4:
                        System.out.println("Skriv inn ord som skal erstattes:");
                        String oldWord = scannerObj.nextLine();
                        System.out.println("Skriv inn ord som skal erstatte:");
                        String newWord = scannerObj.nextLine();
                        System.out.println(text.replaceWord(oldWord, newWord));
                        break;
                    case 5:
                        System.out.println(text.getText());
                        break;
                    case 6:
                        System.out.println(text.getUpperCase());
                        break;
                    default:
                        break;
                }
            }
            else{
                System.out.println("Ugyldig valg");
            }
        }
    }
}
