package Oving8.Opp1Oving8;

import java.util.GregorianCalendar;
import java.util.Scanner;

public class Employee {
    // klasseattributter
    public static GregorianCalendar calendar = new GregorianCalendar();

    // instansattributter
    private Person person;
    private int employeeNumber;
    private int employmentYear;
    private int monthlySalary;
    private int taxRate;

    /**
     * Konstruktør for Employee
     * @param firstName
     * @param lastName
     * @param birthYear
     * @param employeeNumber
     * @param employmentYear
     * @param salary
     * @param taxRate
     */
    public Employee(String firstName, String lastName, int birthYear, int employeeNumber, int employmentYear, int salary, int taxRate){
        this.person = new Person(firstName, lastName, birthYear);
        this.employeeNumber = employeeNumber;
        this.employmentYear = employmentYear;
        this.monthlySalary = salary;
        this.taxRate = taxRate;
    }

    /**
     * Oppretter et nytt Employee-objekt
     * @return Det nye Employee-objektet
     */
    public static Employee createEmployee(){
        Scanner scannerObj = new Scanner(System.in);
        String firstName;
        String lastName;
        int birthYear;
        int employeeNumber;
        int employmentYear ;
        int salary;
        int taxRate;

        System.out.println("For å opprette en ansatt, skriv inn følgende:");
        System.out.print("Fornavn: ");
        firstName = scannerObj.nextLine();
        System.out.print("Etternavn: ");
        lastName = scannerObj.nextLine();
        System.out.print("Fødselsår: ");
        birthYear = scannerObj.nextInt();
        System.out.print("Ansattnummer: ");
        employeeNumber = scannerObj.nextInt();
        System.out.print("Ansettelsesår: ");
        employmentYear = scannerObj.nextInt();
        System.out.print("Månedslønn: ");
        salary = scannerObj.nextInt();
        System.out.print("Skatteprosent (%): ");
        taxRate = scannerObj.nextInt();

        return new Employee(firstName, lastName, birthYear, employeeNumber, employmentYear, salary, taxRate);
    }

    /**
     * Returnerer ansattnummeret
     * @return Ansatt nummer
     */
    public int getEmployeeNumber(){
        return this.employeeNumber;
    }

    /**
     * Returnerer ansettelsesåret
     * @return Ansettelsesår
     */
    public int getEmploymentYear(){
        return this.employmentYear;
    }

    /**
     * Returnerer månedslønnen
     * @return Månedslønn
     */
    public int getMonthlySalary(){
        return this.monthlySalary;
    }

    /**
     * Returnerer skatteprosenten
     * @return Skatteprosent
     */
    public int getTaxRate(){
        return this.taxRate;
    }

    /**
     * Returnerer månedlig skattetrekk
     * @return Månedlig skattetrekk
     */
    public int getMonthlyTax(){
        return this.monthlySalary * this.getTaxRate() / 100;
    }

    /**
     * Returnerer årslønnen
     * @return Årslønn
     */
    public int getYearlySalary(){
        return this.monthlySalary * 12;
    }

    /**
     * Returnerer årlig skattetrekk
     * @return Årlig skattetrekk
     */
    public Double getYearlyTax(){
        return this.getMonthlyTax() * 10.5;
    }

    /**
     * Returnerer navnet til den ansatte
     * @return Navnet til den ansatte
     */
    public String getName(){
        return this.person.getLastName() + ", " + this.person.getFirstName();
    }

    /**
     * Returnerer alderen til den ansatte
     * @return Alderen til den ansatte
     */
    public int getAge(){
        int year = calendar.get(GregorianCalendar.YEAR);
        return year - this.person.getBirthYear();
    }

    /**
     * Returnerer antall år den ansatte har vært ansatt
     * @return Antall år den ansatte har vært ansatt
     */
    public int getYearsEmployed(){
        int year = calendar.get(GregorianCalendar.YEAR);
        return year - this.getEmploymentYear();
    }

    /**
     * Returnerer true hvis den ansatte har vært ansatt i mer enn et gitt antall år
     * @param years Antall år den ansatte må ha vært ansatt i
     * @return True hvis den ansatte har vært ansatt i mer enn et gitt antall år
     */
    public boolean employedMoreThan(int years){
        return this.getYearsEmployed() > years;
    }

    /**
     * Endrer månedslønnen
     * @param salary Ny månedslønn
     */
    public void setMonthlySalary(int salary){
        this.monthlySalary = salary;
    }

    /**
     * Endrer skatteprosenten
     * @param taxRate Ny skatteprosent
     */
    public void setTaxRate(int taxRate){
        this.taxRate = taxRate;
    }

    /**
     * Returnerer en string med informasjon om den ansatte
     * @return String med informasjon om den ansatte
     */
    public String toString(){
        int years = 5;
        return "Navn: " + this.getName() + "\n" +
                "Alder: " + this.getAge() + "\n" +
                "Ansattnummer: " + this.getEmployeeNumber() + "\n" +
                "Ansatt i " + this.getYearsEmployed() + " år\n" +
                "Ansatt for mer enn " + years + " år: " + this.employedMoreThan(years) + "\n" +
                "Månedslønn: " + this.getMonthlySalary() + "\n" +
                "Månedslig skattetrekk: " + this.getMonthlyTax() + "\n" +
                "Årslønn: " + this.getYearlySalary() + "\n" +
                "Årlig skattetrekk " + this.getYearlyTax() + "\n";
    }
}
