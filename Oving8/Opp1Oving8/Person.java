package Oving8.Opp1Oving8;

public class Person {
    // instansattributter
    private String firstName;
    private String lastName;
    private Integer birthYear;

    /**
     * Konstruktør for Person
     * @param firstName
     * @param lastName
     * @param birthYear
     */
    public Person(String firstName, String lastName, Integer birthYear){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = birthYear;
    }

    /**
     * Returnerer fornavnet til personen
     * @return Fornavnet til personen
     */
    public String getFirstName(){
        return this.firstName;
    }

    /**
     * Returnerer etternavnet til personen
     * @return Etternavnet til personen
     */
    public String getLastName(){
        return this.lastName;
    }

    /**
     * Returnerer fødselsåret til personen
     * @return Fødselsåret til personen
     */
    public Integer getBirthYear(){
        return this.birthYear;
    }
}
