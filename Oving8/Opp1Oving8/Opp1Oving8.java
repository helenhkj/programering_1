package Oving8.Opp1Oving8;

import java.util.ArrayList;
import java.util.Scanner;

public class Opp1Oving8 {
    public static void main(String[] args) {
        // variabler
        ArrayList<Employee> employees = new ArrayList<Employee>();
        Scanner scannerObj = new Scanner(System.in);
        int option;
        Employee currentEmployee;

        // meny
        while (true) {
            // hovedmeny
            System.out.println("\nAnsatte:");
            for (int i = 0; i < employees.size(); i++){
                System.out.printf("[%d] %s\n", i+1, employees.get(i).getName());
            }
            System.out.printf("[%d] Legg til ansatt\n", employees.size()+1);
            System.out.println("[0] Avslutt");

            option = scannerObj.nextInt();

            if (option == 0){ // avslutt
                break;
            } else if (option == employees.size()+1){ // legg til ansatt
                employees.add(Employee.createEmployee());
            } else if (option > 0 && option <= employees.size()){ // ansattmeny
                currentEmployee = employees.get(option-1);
                while (true){
                    System.out.println(currentEmployee.toString());
                    System.out.println("[1] Endre Månedslønn");
                    System.out.println("[2] Endre Skatteprosent");
                    System.out.println("[0] Tilbake");
                    option = scannerObj.nextInt();
                    if (option == 0){ // tilbake
                        break;
                    } else if (option == 1){ // endre månedslønn
                        System.out.print("Ny månedslønn: ");
                        currentEmployee.setMonthlySalary(scannerObj.nextInt());
                    } else if (option == 2){ // endre skatteprosent
                        System.out.print("Ny skatteprosent: ");
                        currentEmployee.setTaxRate(scannerObj.nextInt());
                    } else {
                        System.out.println("Ugyldig valg");
                    }
                }
            } else {
                System.out.println("Ugyldig valg");
            }
        }
    }
}
